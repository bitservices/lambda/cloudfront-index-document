"use strict";
exports.handler = (event, context, callback) => {

  // The index document name
  var strIndexDocument = "index.html";

  // Extract the request from the CloudFront event that is sent to Lambda@Edge
  var reqReqest = event.Records[0].cf.request;

  // Extract the URI from the request
  var uriOld = reqReqest.uri;

  // Match any '/' that occurs at the end of a URI. Replace it with a default index
  var uriNew = uriOld.replace(/\/$/, "\/" + strIndexDocument);

  // Log the URI as received by CloudFront and the new URI to be used to fetch from origin
  // console.log("Old URI: " + uriOld);
  // console.log("New URI: " + uriNew);

  // Replace the received URI with the URI that includes the index page
  reqReqest.uri = uriNew;

  // Return to CloudFront
  return callback(null, reqReqest);
};
